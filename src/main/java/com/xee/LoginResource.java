package com.xee;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Login action
 * Redirect to the OAuth Server
 *
 * @author Xee
 */
@Path("/login")
public class LoginResource {

    private Logger logger = Logger.getLogger(LoginResource.class.getSimpleName());
    private static final Properties properties = new Properties();

    @GET
    public Response login() throws IOException, URISyntaxException {
        if (properties.isEmpty()) {
            properties.load(LoginResource.class.getResourceAsStream("/config.properties"));
        }

        String redirectUriString = URLEncoder.encode(properties.getProperty("app.redirecturi"), "UTF-8");
        String scope = URLEncoder.encode(properties.getProperty("app.scope"), "UTF-8");

        URI redirectUri = new URI(properties.getProperty("oauth.server")
                + "client_id=" + properties.getProperty("app.clientid")
                + "&scope=" + scope
                + "&redirect_uri=" + redirectUriString);
        return Response.temporaryRedirect(redirectUri).build();
    }

}