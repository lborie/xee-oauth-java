# Xee OAuth Skeleton

Xee OAuth Skeleton est un squelette d'application pour créer et surtout déployer son Squelette d'application Xee en quelques minutes sur Google Appengine.
Mais sur le principe, cette application génère un WAR standard, déployable dans n'importe quel conteneur d'application.

Quelles sont les étapes ?

### Création d'un projet Google Appengine
Aller sur la [console Google Cloud Platform](https://console.developers.google.com/project) et créer un nouveau projet.
Attention à l'identifiant du projet.

### Edition des configurations
Editer le fichier appengine-web.xml et remplir <application>xee-skeleton</application> avec son identifiant de projet Appengine.
Le projet est basé sur [Jersey](https://jersey.java.net/), implémentation du standard Jax-RS pour créer des APIs.

Editer le fichier config.properties et remplir avec ses propriétés d'application Xee.

### Lancemenent en local
Grâce au [plugin maven](https://cloud.google.com/appengine/docs/java/tools/maven), à la racine,
` mvn clean package ` puis ` mvn appengine:devserver `

### Déploiement
` mvn appengine:update `
Votre application est ensuite disponible sur http://<IDENTIFIANT>.appspot.com
